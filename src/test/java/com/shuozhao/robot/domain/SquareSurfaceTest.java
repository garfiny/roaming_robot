package com.shuozhao.robot.domain;

import org.junit.Test;

import static org.junit.Assert.*;

public class SquareSurfaceTest {

    private final static int SURFACE_SIZE = 10;
    private SquareTableSurface surface = new SquareTableSurface(SURFACE_SIZE);

    @Test
    public void testIsValidMove() {
        for (int i = 0; i < SURFACE_SIZE; i++) {
            for (int j = 0; j < SURFACE_SIZE; j++) {
                assertTrue(surface.isOnSurface(new Position(i, j, Orientation.EAST)));
            }
        }
    }

    @Test
    public void testIsValidMoveWithInvalidAxes() {
        assertFalse(surface.isOnSurface(
                new Position(SURFACE_SIZE + 1, SURFACE_SIZE + 1, Orientation.EAST)));
        assertFalse(surface.isOnSurface(
                new Position(-1, SURFACE_SIZE + 1, Orientation.EAST)));
        assertFalse(surface.isOnSurface(
                new Position(SURFACE_SIZE + 1, -1, Orientation.EAST)));
        assertFalse(surface.isOnSurface(new Position(-1, -1, Orientation.EAST)));
    }
}