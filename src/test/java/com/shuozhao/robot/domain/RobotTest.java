package com.shuozhao.robot.domain;

import org.junit.Test;

import static org.junit.Assert.*;

public class RobotTest {

    @Test
    public void testIsPlaced() {
        Robot robot = new Robot();
        robot.setPosition(new Position(1, 1, Orientation.EAST));
        assertTrue(robot.isPlaced());
    }

    @Test
    public void testIsPlacedForNewRobot() {
        Robot robot = new Robot();
        assertFalse(robot.isPlaced());
    }

}