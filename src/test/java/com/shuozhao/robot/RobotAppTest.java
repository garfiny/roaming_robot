package com.shuozhao.robot;

import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import java.io.*;

import static org.junit.Assert.*;

public class RobotAppTest {

    private InputStream sysIn;
    private OutputStream outStream = new ByteArrayOutputStream();
    private RobotApp app;

    @Rule
    public ExpectedException thrown = ExpectedException.none();

    @Before
    public void setup() throws InstantiationException, IllegalAccessException {
        app = new RobotApp();
        sysIn = System.in;
        System.setOut(new PrintStream(outStream));
    }

    @After
    public void tearDown() throws Exception {
        System.setIn(sysIn);
        System.setOut(null);
    }

    @Test
    public void testInputCommandsSet1() {
        String[] commands = new String[] {
                "PLACE 0,0,NORTH",
                "MOVE",
                "REPORT", "\n"
        };
        String output = "Output: 0,1,NORTH\n";
        testAppWith(commands, output);
    }

    @Test
    public void testInputCommandsSet2() {
        String[] commands = new String[] {
                "PLACE 0,0,NORTH",
                "LEFT",
                "REPORT", "\n"
        };
        String output = "Output: 0,0,WEST\n";
        testAppWith(commands, output);
    }

    @Test
    public void testInputCommandsSet3() {
        String[] commands = new String[] {
                "PLACE 1,2,EAST",
                "MOVE",
                "MOVE",
                "LEFT",
                "MOVE",
                "REPORT", "\n"
        };
        String output = "Output: 3,3,NORTH\n";
        testAppWith(commands, output);
    }

    @Test
    public void testInputCommandsSet4() {
        String[] commands = new String[] {
                "MOVE",
                "LEFT",
                "RIGHT",
                "REPORT",
                "PLACE 1,2,EAST",
                "MOVE",
                "MOVE",
                "LEFT",
                "MOVE",
                "REPORT", "\n"
        };
        String output = "Output: 3,3,NORTH\n";
        testAppWith(commands, output);
    }

    @Test
    public void testInputCommandsSet5() {
        String[] commands = new String[] {
                "ROAM",
                "LEFT",
                "NONE",
                "REPORT",
                "PLACE 1,2,EAST",
                "MOVE",
                "MOVE",
                "LEFT",
                "MOVE",
                "REPORT", "\n"
        };
        String output = "Output: 3,3,NORTH\n";
        testAppWith(commands, output);
    }

    private void testAppWith(String[] commands, String output) {
        String input = String.join("\n", commands);
        ByteArrayInputStream in = new ByteArrayInputStream(input.getBytes());
        System.setIn(in);
        app.play();
        assertEquals(output, outStream.toString());
    }
}