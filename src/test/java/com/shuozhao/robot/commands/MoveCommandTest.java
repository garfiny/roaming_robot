package com.shuozhao.robot.commands;

import com.shuozhao.robot.domain.Orientation;
import com.shuozhao.robot.domain.Position;
import org.junit.Test;

import static org.junit.Assert.*;

public class MoveCommandTest {

    private Command command = new MoveCommand();

    @Test
    public void testCommandName() {
        assertEquals("MOVE", command.name());
    }

    @Test
    public void testExecuteWithNorthFacing() throws InvalidCommandException {
        Position position = new Position(3, 3, Orientation.NORTH);
        Position newPos = command.execute(position);
        assertEquals(position.getOrientation(), newPos.getOrientation());
        assertEquals(position.getX(), newPos.getX());
        assertEquals(position.getY() + 1, newPos.getY());
    }

    @Test
    public void testExecuteWithSouthFacing() throws InvalidCommandException {
        Position position = new Position(3, 3, Orientation.SOUTH);
        Position newPos = command.execute(position);
        assertEquals(position.getOrientation(), newPos.getOrientation());
        assertEquals(position.getX(), newPos.getX());
        assertEquals(position.getY() - 1, newPos.getY());
    }

    @Test
    public void testExecuteWithEastFacing() throws InvalidCommandException {
        Position position = new Position(3, 3, Orientation.EAST);
        Position newPos = command.execute(position);
        assertEquals(position.getOrientation(), newPos.getOrientation());
        assertEquals(position.getX() + 1, newPos.getX());
        assertEquals(position.getY(), newPos.getY());
    }

    @Test
    public void testExecuteWithWestFacing() throws InvalidCommandException {
        Position position = new Position(3, 3, Orientation.WEST);
        Position newPos = command.execute(position);
        assertEquals(position.getOrientation(), newPos.getOrientation());
        assertEquals(position.getX() - 1, newPos.getX());
        assertEquals(position.getY(), newPos.getY());
    }
}