package com.shuozhao.robot.commands;

import com.shuozhao.robot.domain.Orientation;
import com.shuozhao.robot.domain.Position;
import org.junit.Test;

import static org.junit.Assert.*;

public class LeftCommandTest {

    private Command command = new LeftCommand();

    @Test
    public void testCommandName() {
        assertEquals("LEFT", command.name());
    }

    @Test
    public void testExecuteWithNorthFacing() throws InvalidCommandException {
        Position position = new Position(3, 3, Orientation.NORTH);
        Position newPos = command.execute(position);
        assertEquals(position.getX(), newPos.getX());
        assertEquals(position.getY(), newPos.getY());
        assertEquals(Orientation.WEST, newPos.getOrientation());
    }

    @Test
    public void testExecuteWithSouthFacing() throws InvalidCommandException {
        Position position = new Position(3, 3, Orientation.SOUTH);
        Position newPos = command.execute(position);
        assertEquals(position.getX(), newPos.getX());
        assertEquals(position.getY(), newPos.getY());
        assertEquals(Orientation.EAST, newPos.getOrientation());
    }

    @Test
    public void testExecuteWithEastacing() throws InvalidCommandException {
        Position position = new Position(3, 3, Orientation.EAST);
        Position newPos = command.execute(position);
        assertEquals(position.getX(), newPos.getX());
        assertEquals(position.getY(), newPos.getY());
        assertEquals(Orientation.NORTH, newPos.getOrientation());
    }

    @Test
    public void testExecuteWithWestFacing() throws InvalidCommandException {
        Position position = new Position(3, 3, Orientation.WEST);
        Position newPos = command.execute(position);
        assertEquals(position.getX(), newPos.getX());
        assertEquals(position.getY(), newPos.getY());
        assertEquals(Orientation.SOUTH, newPos.getOrientation());
    }
}