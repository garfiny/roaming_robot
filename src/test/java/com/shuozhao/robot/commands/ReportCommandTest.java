package com.shuozhao.robot.commands;

import com.shuozhao.robot.domain.Orientation;
import com.shuozhao.robot.domain.Position;
import org.junit.Test;

import java.io.ByteArrayOutputStream;
import java.io.OutputStream;
import java.io.PrintStream;

import static org.junit.Assert.*;

public class ReportCommandTest {

    private Command command = new ReportCommand();

    @Test
    public void testExecute() throws InvalidCommandException {
        Position position = new Position(3, 3, Orientation.EAST);
        OutputStream outStream = new ByteArrayOutputStream();
        System.setOut(new PrintStream(outStream));
        Position newPos = command.execute(position);
        assertEquals("Output: 3,3,EAST\n", outStream.toString());
        assertEquals(position.getX(), newPos.getX());
        assertEquals(position.getY(), newPos.getY());
        assertEquals(position.getOrientation(), newPos.getOrientation());
    }
}