package com.shuozhao.robot.commands;

import com.shuozhao.robot.domain.Orientation;
import com.shuozhao.robot.domain.Position;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import static org.junit.Assert.*;

public class PlaceCommandTest {

    private PlaceCommand command = new PlaceCommand();

    @Rule
    public ExpectedException thrown = ExpectedException.none();

    @Test
    public void testCommandName() {
        assertEquals("PLACE", command.name());
    }

    @Test
    public void testExecuteWithValidInput() throws InvalidCommandException {
        Position position = command.execute(null, "1", "1", "NORTH");
        assertEquals(1, position.getX());
        assertEquals(1, position.getY());
        assertEquals(Orientation.NORTH, position.getOrientation());
    }

    @Test
    public void testExecuteWithWrongNumberOfArguments() throws InvalidCommandException {
        thrown.expect(InvalidCommandException.class);
        command.execute(null, "1", "NORTH");
    }

    @Test
    public void testExecuteWithWrongNumberFormat() throws InvalidCommandException {
        thrown.expect(InvalidCommandException.class);
        command.execute(null, "A", "1", "NORTH");
    }

    @Test
    public void testExecuteWithWrongOrientation() throws InvalidCommandException {
        thrown.expect(InvalidCommandException.class);
        command.execute(null, "1", "1", "UNDER");
    }
}