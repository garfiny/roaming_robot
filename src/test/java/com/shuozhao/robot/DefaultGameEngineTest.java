package com.shuozhao.robot;

import com.shuozhao.robot.commands.*;
import com.shuozhao.robot.domain.Orientation;
import com.shuozhao.robot.domain.Position;
import com.shuozhao.robot.domain.Robot;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import static org.junit.Assert.*;

public class DefaultGameEngineTest {

    private GameEngine engine = new DefaultGameEngine();

    @Rule
    public ExpectedException thrown = ExpectedException.none();

    @Test
    public void testGetCommandWithValidString() throws InstantiationException, IllegalAccessException {
        engine.registerCommand(LeftCommand.class);
        Command command = engine.getCommand("LEFT").get();
        assertEquals(LeftCommand.class, command.getClass());
    }

    @Test
    public void testGetCommandWithUnsupportedCommand() throws InstantiationException, IllegalAccessException {
        assertFalse(engine.getCommand("unsupported_command").isPresent());
    }

    @Test
    public void testCommandArgumentsWithEmptyInput() {
        String[] arguments = engine.commandArguments("LEFT");
        assertNull(arguments);
    }

    @Test
    public void testCommandArgumentsWithInput() {
        String[] arguments = engine.commandArguments("PLACE 0,0,NORTH");
        assertEquals("0", arguments[0]);
        assertEquals("0", arguments[1]);
        assertEquals("NORTH", arguments[2]);
    }

    @Test
    public void testExecuteCommand() throws InvalidCommandException {
        Robot robot = new Robot();
        robot.setPosition(new Position(0,0, Orientation.EAST));
        Command command = new LeftCommand();
        engine.execute(robot, command, null);
        assertEquals(0, robot.getPosition().getX());
        assertEquals(0, robot.getPosition().getY());
        assertEquals(Orientation.NORTH, robot.getPosition().getOrientation());
    }

    @Test
    public void testExecuteCommandWhenRobotIsNotPlaced() throws InvalidCommandException {
        Robot robot = new Robot();
        Command command = new LeftCommand();
        engine.execute(robot, command, null);
        assertFalse(robot.isPlaced());
    }

    @Test
    public void testExecutePlaceCommand() throws InvalidCommandException {
        Robot robot = new Robot();
        Command command = new PlaceCommand();
        engine.execute(robot, command, new String[]{"0", "0", "NORTH"});
        assertEquals(0, robot.getPosition().getX());
        assertEquals(0, robot.getPosition().getY());
        assertEquals(Orientation.NORTH, robot.getPosition().getOrientation());
    }

    @Test
    public void testExecuteCommandWithInvalidMove() throws InvalidCommandException {
        Robot robot = new Robot();
        Position position = new Position(0, 0, Orientation.WEST);
        robot.setPosition(position);
        Command command = new MoveCommand();
        engine.execute(robot, command, null);
        assertEquals(position, robot.getPosition());
    }

    @Test
    public void testRegisterCommand() throws InstantiationException, IllegalAccessException, InvalidCommandException {
        engine.registerCommand(LeftCommand.class);
        Command command = engine.getCommand("LEFT").get();
        assertEquals(LeftCommand.class, command.getClass());
    }
}