package com.shuozhao.robot;

import com.shuozhao.robot.commands.Command;
import com.shuozhao.robot.commands.InvalidCommandException;
import com.shuozhao.robot.commands.LeftCommand;
import com.shuozhao.robot.domain.Robot;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import java.util.Optional;
import static org.mockito.Mockito.*;

public class GameEngineTest {

    private GameEngine engine = spy(new GameEngine() {
        @Override
        public Optional<Command> getCommand(String str) {
            return null;
        }

        @Override
        public String[] commandArguments(String commandStr) {
            return new String[0];
        }

        @Override
        public void execute(Robot robot, Command command, String[] arguments) throws InvalidCommandException {

        }

        @Override
        public void registerCommand(Class<? extends Command> clazz) throws IllegalAccessException, InstantiationException {

        }
    });

    @Rule
    public ExpectedException thrown = ExpectedException.none();

    @Test
    public void testPlay() throws InvalidCommandException {
        String commandStr = "LEFT";
        Robot robot = new Robot();
        LeftCommand command = new LeftCommand();
        when(engine.getCommand(commandStr)).thenReturn(Optional.of(command));
        when(engine.commandArguments(commandStr)).thenReturn(null);
        engine.play(robot, commandStr);
        verify(engine, times(1)).getCommand(commandStr);
        verify(engine, times(1)).commandArguments(commandStr);
        verify(engine, times(1)).execute(robot, command, null);
    }

    @Test
    public void testPlayWithUnsupportedCommand() throws InvalidCommandException {
        String commandStr = "unsupported";
        Robot robot = new Robot();
        when(engine.getCommand(commandStr)).thenReturn(Optional.empty());
        thrown.expect(InvalidCommandException.class);
        engine.play(robot, commandStr);
        verify(engine, times(1)).getCommand(commandStr);
        verify(engine, never()).commandArguments(commandStr);
        verify(engine, never()).execute(robot, any(Command.class), null);
    }
}
