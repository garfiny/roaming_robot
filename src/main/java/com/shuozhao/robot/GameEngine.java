package com.shuozhao.robot;

import com.shuozhao.robot.commands.Command;
import com.shuozhao.robot.commands.InvalidCommandException;
import com.shuozhao.robot.domain.Robot;

import java.util.Optional;

public interface GameEngine {

    default void play(Robot robot, String commandStr) throws InvalidCommandException {
        Optional<Command> command = getCommand(commandStr);
        if (command.isPresent()) {
            String[] arguments = commandArguments(commandStr);
            execute(robot, command.get(), arguments);
        } else {
            throw new InvalidCommandException("Unsupported Command: " + commandStr);
        }
    }

    Optional<Command> getCommand(String str);

    String[] commandArguments(String commandStr);

    void execute(Robot robot, Command command, String[] arguments) throws InvalidCommandException;

    void registerCommand(Class<? extends Command> clazz) throws IllegalAccessException, InstantiationException;
}
