package com.shuozhao.robot;

import com.shuozhao.robot.commands.Command;
import com.shuozhao.robot.commands.InvalidCommandException;
import com.shuozhao.robot.domain.Position;
import com.shuozhao.robot.domain.Robot;
import com.shuozhao.robot.domain.SquareTableSurface;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

public class DefaultGameEngine implements GameEngine {

    public static final int SQUARE_SURFACE_SIZE = 5;
    private SquareTableSurface surface;
    private Map<String, Command> supportedCommands;

    public DefaultGameEngine() {
        surface = new SquareTableSurface(SQUARE_SURFACE_SIZE);
        supportedCommands = new HashMap<>();
    }

    @Override
    public Optional<Command> getCommand(String str) {
        String[] strings = str.split(" ");
        Command command = supportedCommands.get(strings[0]);
        if (command == null)
            return Optional.empty();
        else
            return Optional.of(command);
    }

    @Override
    public String[] commandArguments(String commandStr) {
        String[] strings = commandStr.split(" ");
        if (strings.length > 1) {
            return strings[1].split(",");
        } else {
            return null;
        }
    }

    @Override
    public void execute(Robot robot, Command command, String[] arguments) throws InvalidCommandException {
        if (!robot.isPlaced() && !"PLACE".equals(command.name())) {
            return;
        }
        Position position = command.execute(robot.getPosition(), arguments);
        if (surface.isOnSurface(position))
            robot.setPosition(position);
    }

    @Override
    public void registerCommand(Class<? extends Command> clazz) throws IllegalAccessException, InstantiationException {
        Command command = clazz.newInstance();
        supportedCommands.put(command.name(), command);
    }
}
