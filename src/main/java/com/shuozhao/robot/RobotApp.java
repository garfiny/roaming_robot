package com.shuozhao.robot;

import com.shuozhao.robot.commands.*;
import com.shuozhao.robot.domain.Robot;

import java.util.Scanner;

public class RobotApp {

    private DefaultGameEngine gameEngine;

    public RobotApp() throws IllegalAccessException, InstantiationException {
        this.gameEngine = new DefaultGameEngine();
        registerCommands();
    }

    private void registerCommands() throws InstantiationException, IllegalAccessException {
        this.gameEngine.registerCommand(LeftCommand.class);
        this.gameEngine.registerCommand(RightCommand.class);
        this.gameEngine.registerCommand(PlaceCommand.class);
        this.gameEngine.registerCommand(MoveCommand.class);
        this.gameEngine.registerCommand(ReportCommand.class);
    }

    public void play() {
        Robot robot = new Robot();
        Scanner scanner = new Scanner(System.in);
        while (true) {
            String line = scanner.nextLine();
            if (line == null || line.isEmpty()) {
                return;
            }
            try {
                gameEngine.play(robot, line);
            } catch (InvalidCommandException e) {
            }
        }
    }

    public static void main(String[] args) {
        try {
            new RobotApp().play();
        } catch (Exception e) {
            System.out.println("Internal Error!");
            e.printStackTrace();
        }
    }
}
