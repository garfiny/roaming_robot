package com.shuozhao.robot.domain;

public class Robot {

    private Position position;

    public Robot() {}

    public Position getPosition() {
        return position;
    }

    public void setPosition(Position position) {
        this.position = position;
    }

    public boolean isPlaced() {
        return position != null;
    }
}
