package com.shuozhao.robot.domain;

public class SquareTableSurface {

    private int size;

    public SquareTableSurface(int size) {
        this.size = size;
    }

    public boolean isOnSurface(Position position) {
        return isValidAxis(position.getX()) && isValidAxis(position.getY());
    }

    private boolean isValidAxis(int n) {
        return n >= 0 && n <= (size - 1);
    }
}