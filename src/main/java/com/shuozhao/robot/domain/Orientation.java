package com.shuozhao.robot.domain;

public enum Orientation {
    NORTH, SOUTH, EAST, WEST
}