package com.shuozhao.robot.commands;

import com.shuozhao.robot.domain.Orientation;
import com.shuozhao.robot.domain.Position;

public class RightCommand implements Command {

    @Override
    public String name() {
        return "RIGHT";
    }

    @Override
    public Position execute(Position position, String... args) throws InvalidCommandException {
        Orientation newOrientation = null;
        switch (position.getOrientation()) {
            case EAST:
                newOrientation = Orientation.SOUTH;
                break;
            case NORTH:
                newOrientation = Orientation.EAST;
                break;
            case SOUTH:
                newOrientation = Orientation.WEST;
                break;
            case WEST:
                newOrientation = Orientation.NORTH;
                break;
        }
        return new Position(position.getX(), position.getY(), newOrientation);
    }
}
