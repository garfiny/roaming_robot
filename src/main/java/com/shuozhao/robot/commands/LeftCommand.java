package com.shuozhao.robot.commands;

import com.shuozhao.robot.domain.Orientation;
import com.shuozhao.robot.domain.Position;

public class LeftCommand implements Command {

    @Override
    public String name() {
        return "LEFT";
    }

    @Override
    public Position execute(Position position, String... args) throws InvalidCommandException {
        Orientation newOrientation = null;
        switch (position.getOrientation()) {
            case EAST:
                newOrientation = Orientation.NORTH;
                break;
            case NORTH:
                newOrientation = Orientation.WEST;
                break;
            case SOUTH:
                newOrientation = Orientation.EAST;
                break;
            case WEST:
                newOrientation = Orientation.SOUTH;
                break;
        }
        return new Position(position.getX(), position.getY(), newOrientation);
    }
}
