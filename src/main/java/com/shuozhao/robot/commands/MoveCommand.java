package com.shuozhao.robot.commands;

import com.shuozhao.robot.domain.Position;

public class MoveCommand implements Command {

    @Override
    public String name() {
        return "MOVE";
    }

    @Override
    public Position execute(Position position, String... args) throws InvalidCommandException {
        int newX = position.getX();
        int newY = position.getY();
        switch (position.getOrientation()) {
            case EAST:
                newX += 1;
                break;
            case NORTH:
                newY += 1;
                break;
            case SOUTH:
                newY -= 1;
                break;
            case WEST:
                newX -= 1;
                break;
        }
        return new Position(newX, newY, position.getOrientation());
    }
}
