package com.shuozhao.robot.commands;

import com.shuozhao.robot.domain.Orientation;
import com.shuozhao.robot.domain.Position;

public class PlaceCommand implements Command {

    @Override
    public String name() {
        return "PLACE";
    }

    @Override
    public Position execute(Position position, String... args) throws InvalidCommandException {
        try {
            if (args.length != 3) {
                throw new InvalidCommandException("Invalid positions, arguments should be X,Y,Orientation!");
            }
            int x = Integer.parseInt(args[0]);
            int y = Integer.parseInt(args[1]);
            Orientation orientation = Orientation.valueOf(args[2]);
            return new Position(x, y, orientation);
        } catch (NumberFormatException nx) {
           throw new InvalidCommandException("Input positions is invalid: [x = " + args[0] + ", y = " + args[1] + "]");
        } catch (IllegalArgumentException ix) {
            throw new InvalidCommandException("Input Orientation is invalid: " + args[2]);
        }
    }
}
