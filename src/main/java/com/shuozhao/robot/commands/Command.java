package com.shuozhao.robot.commands;

import com.shuozhao.robot.domain.Position;

public interface Command {

    String name();

    Position execute(Position position, String... args) throws InvalidCommandException;
}
