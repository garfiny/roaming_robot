package com.shuozhao.robot.commands;

import com.shuozhao.robot.domain.Position;

public class ReportCommand implements Command {

    @Override
    public String name() {
        return "REPORT";
    }

    @Override
    public Position execute(Position position, String... args) throws InvalidCommandException {
        String s = String.format("Output: %1d,%1d,%s",
                position.getX(), position.getY(),
                position.getOrientation());
        System.out.println(s);
        return position;
    }
}
