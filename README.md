## Build

* run - ./gradlew build

##  Run

* run - java -jar build/libs/robot-1.0-SNAPSHOT.jar
* Example Input:
    - PLACE 0,0,NORTH
    - MOVE
    - REPORT
* Input an empty line to quit the app
